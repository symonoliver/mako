# ============= #
# FCast Rev 1.0 #
# ============= #

# NOTES:

# Nowcast
# url = "http://tds.glos.us/thredds/dodsC/glos/glcfs/archivecurrent/erie/ncfmrc-2d/files/e201800100.out1.nc"

# Timezones
# print datetime.datetime.now(pytz.timezone("GMT"))

# Forecast via http://tds.glos.us/thredds/catalog.html
# Data rights: No usage restrictions

# e201810412.out1.nc (x = first initial of lake name, 0000 = year, )

# twice a day every 12 hours.

# 2018 104 12 YYYY DDD HH - there are 365 days

# 00 = GMT: Saturday, 14 April 2018 1:00:00 AM - Thursday, 19 April 2018 12:00:00 AM = 119 hours (120 entries)
# 12 = GMT: Saturday, 14 April 2018 1:00:00 PM - Thursday, 19 April 2018 12:00:00 PM = 119 hours (120 entries)

# Forecasts updated by about 01:30 (1:30 AM) and 13:30 (1:30 PM) GMT (subtract 4 for EDT, 5 for EST)


import matplotlib.pyplot as plt
import matplotlib, pylab
import numpy as np
import netCDF4
import datetime
import pytz
import scipy.ndimage as ndimage
import os, errno
import matplotlib.colors as col

#from __future__ import print_function
from PIL import Image
import os, sys
from os import listdir
import os.path
from os.path import isfile, join


class cl:
    OKGREEN = '\033[92m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    OKBLUE = '\033[94m'


def nf(value):
	feet = value*3.28084
	return int(feet)


def getData(lakeName):

	print lakeName

	url = createLakeCode(lakeName)

	print url

	dictData = {}

	try:
		#Use NetCDF 4 to download the dataset into memory
		nc = netCDF4.Dataset(url)
		print cl.OKGREEN+"Data Success!"+cl.ENDC

	except RuntimeError:
		print cl.FAIL+"File not found."+cl.ENDC

	#Retrieve specific variables from dataset
	lon = nc.variables['lon']
	lat = nc.variables['lat']
	wvh = nc.variables['wvh']
	wvd = nc.variables['wvd']
	time = nc.variables['time']

	dictData['lon'] = lon[:].squeeze()
	dictData['lat'] = lat[:].squeeze()
	dictData['wvh'] = (wvh[:,:,:]*3.28).squeeze() #convert from meters to feet before squeeze
	dictData['wvd'] = wvd[:]
	dictData['time'] = time[:].squeeze()
	dictData['wvh'] = ndimage.gaussian_filter(np.ma.masked_invalid(dictData['wvh']),sigma=0.0005, order=0, mode="nearest", cval=0.05)

	nc.close()

	return dictData


def completeLakes(lake,lakename):

	print cl.OKBLUE+"Drawing: "+lakename+cl.ENDC

	counter = 0

	for topo in lake['wvh']:

		fig = plt.figure(frameon=False)

		cs = fig.add_subplot(111)
		cs.set_aspect(1.5)

		clevs = np.arange(0.0, 11, 0.5) #loose gradient levels
		#clevs = np.arange(0.0, 10, 0.1) #tighter gradient levels

		#print clevs

		norm = col.BoundaryNorm(clevs, 256)

		#cs.contourf(lake['lon'],lake['lat'],lake['wvh'][counter],clevs,cmap='jet',norm=norm, vmin=0, vmax=17.0, origin='lower')
		#cs.contour(eri['x'],eri['y'],eri['z'][counter],clevs,cmap='jet', linewidths=0.5)
		waves1 = cs.contour(lake['lon'],lake['lat'],lake['wvh'][counter],clevs,colors='k', linewidths=0.25)
		waves2 = cs.contourf(lake['lon'],lake['lat'],lake['wvh'][counter],clevs,cmap='jet',norm=norm, vmin=0, vmax=16)
		
		# plt.contour(CS, levels=CS.levels[::2],
  #                 colors='r',
  #                 origin=origin)

		#cbar = plt.colorbar(waves2)
		#cbar.ax.set_ylabel('wave height (ft)')

		#plt.clabel(waves, inline=1, fontsize=10) #add contour labels

		plt.axis('off') #in this case the axis represent lat and lon

		wvhModel = "%d-%s.png" % (counter,lakename)

		fig.savefig("fcast/"+lakename[0]+"/"+wvhModel, bbox_inches='tight',transparent=True,pad_inches=0,dpi=400)

		print wvhModel

		plt.close()

		counter += 1

		if counter >= 120:
			return

	print cl.OKGREEN+"Done."+cl.ENDC


def createLakeCode(lakename):

	lakeInitial = lakename[0]

	#Datetime to create lake timecode for the request
	now = datetime.datetime.utcnow().timetuple().tm_hour

	print now

	if now >= 2 and now < 14:
		# print cl.OKGREEN+"Data should be available"+cl.ENDC
		lyear = str(datetime.datetime.utcnow().timetuple().tm_year)
		lday = str(datetime.datetime.utcnow().timetuple().tm_yday)

		ltimecode = lakeInitial+lyear+lday+"00.out1.nc"
		lakecode = "http://tds.glos.us/thredds/dodsC/glos/glcfs/"+lakename+"/fcfmrc-2d/files/"+ltimecode

		return lakecode

	elif now > 14:
		# print cl.OKGREEN+"Data should be available"+cl.ENDC
		lyear = str(datetime.datetime.utcnow().timetuple().tm_year)
		lday = str(datetime.datetime.utcnow().timetuple().tm_yday)

		ltimecode = lakeInitial+lyear+lday+"12.out1.nc"
		lakecode = "http://tds.glos.us/thredds/dodsC/glos/glcfs/"+lakename+"/fcfmrc-2d/files/"+ltimecode

		return lakecode


def spriteGrid(lakename,width,height):

	print cl.OKBLUE+"Creating sprite: "+lakename+"..."+cl.ENDC

	files = os.listdir("fcast/"+lakename[0]+"/")
	files.sort()

	print files

	script_dir = os.path.dirname(os.path.abspath(__file__))
	print script_dir

	script_dir = os.path.join(script_dir, "fcast/"+lakename[0]+"/")
	print script_dir
	
	result = Image.new("RGBA", ((7*width), 7*height))

	for index, file in enumerate(files):
	  path = os.path.expanduser(file)
	  img = Image.open(os.path.join(script_dir, path))
	  img.thumbnail((width, height), Image.ANTIALIAS)
	  x = index // 7 * width
	  y = index % 7 * height
	  w, h = img.size
	  print('pos {0},{1} size {2},{3}'.format(x, y, w, h))
	  result.paste(img, (x, y, x + w, y + h))

	result.save(os.path.expanduser("sprites/"+lakename[0]+"/image.png"))



# This is the main loop

erie = getData("erie")
completeLakes(erie,"erie")
spriteGrid("erie",2150,1089)

# huron = getData("huron")
# completeLakes(huron,"huron")
# spriteGrid("huron",1678,1574)

# superior = getData("superior")
# completeLakes(superior,"superior")
# spriteGrid("superior",2150,1072)

# michigan = getData("michigan")
# completeLakes(michigan,"michigan")
# spriteGrid("michigan",849,1574)

# ontario = getData("ontario")
# completeLakes(ontario,"ontario")
# spriteGrid("ontario",2150,965)


exit()